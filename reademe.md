# Lottie-web example 

This uses Adobe After Effects + a plugin called "Bodymovin" to generate a JSON file full of vector data and animation timing instructions. 

You pass that JSON file to a library called Lottie and it does the rest, creating an SVG (or html canvas) animation for your page. 